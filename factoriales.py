#programa 1 - factoriales del 1 al 10

for num in range(1, 10):
    i = 1
    factorial = 1
    while i <= num:
        factorial = factorial * i
        i = i + 1
    print("El factorial de", num, "es:", factorial)